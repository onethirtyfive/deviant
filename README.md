Sample code written by Joshua Morris on 6/2/2012.

The coding assignment is to write a function that calculates
standard deviation. I am gonna write this as a Ruby mixin
module, so that it's reusable on any eumerable object. Writing a function
is fine, but in a heavily object-oriented language like Ruby, it's also
useful to see how I'd approach the problem in a truly usable way.

Please read the feature files in <tt>features</tt> to get a non-technical
idea of what this code does. Coders, check out the <tt>specs</tt>
for an idea of what's going on at object level.

Run everything:

    $ git clone https://bitbucket.org/onethirtyfive/deviant.git
    $ cd deviant
    $ bundle install (assumes you have bundler)
    $ bundle exec cucumber (run features)
    $ bundle exec rspec spec (run specifications)

After running rspec, checkout <tt>coverage/index.html</tt> in a browser.

For more direct usage examples, see <tt>usage.rb</tt>.
