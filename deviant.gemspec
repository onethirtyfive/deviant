# encoding: utf-8
lib = File.expand_path('../lib/', __FILE__)
$:.unshift lib unless $:.include?(lib)
 
require 'deviant/version'

Gem::Specification.new do |s|
  s.name        = 'deviant'
  s.version     = Deviant::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ['Joshua Morris']
  s.email       = ['joshua.a.morris@gmail.com']
  s.homepage    = 'https://bitbucket.org/onethirtyfive/deviant'
  s.summary     = 'Standard deviations toolkit'
  s.description = 'A humble library for calculating standard deviations on an enumerable class'
 
  s.required_rubygems_version = '>= 1.8.11' # This is just the version of rubygems I have.
 
  s.add_development_dependency 'cucumber'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'simplecov'
 
  s.files        = Dir.glob('{bin,lib}/**/*') + %w(LICENSE README.md)
  s.require_path = 'lib'
end
