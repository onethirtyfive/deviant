# encoding: utf-8

$: << File.join(File.dirname(__FILE__), 'lib')
require 'deviant'

def log(*args)
  args.each { |arg| puts "> #{arg}" }
end

data_points = [1,2,3,4,5,10]

puts "Array-based standard deviations on a population:"
log data_points.sigma, data_points.𝜎, data_points.population_stddev

puts "Array-based standard deviations on a sample:"
log data_points.s, data_points.𝑠, data_points.sample_stddev

puts "An enumerator which doubles its members while iterating:"
doubler_klass = Class.new do
  include Deviant

  def initialize(*args)
    @members = args
  end

  def each(&block)
    @members.each { |member| yield (member * 2) } if block_given?
  end
end

doubler = doubler_klass.new(*data_points)
log doubler.sigma

puts "From a file:"
file = File.open('values.txt')
data_points = file.lines.collect(&:to_f)
log data_points.sigma

puts "From a weird distribution:"
data_points = 1.upto(1000).collect { |i| Random.rand(i) }
log data_points.sigma

