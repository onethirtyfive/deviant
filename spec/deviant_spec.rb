require 'spec_helper'

describe 'deviant' do
  let(:example_data_set)   { [170, 300, 430, 470, 600] }
  let(:example_pop_stddev) { 147 }

  describe 'calculations' do
    subject { example_data_set }

    describe '#sigma on the population' do
      it 'calculates variance' do
        subject.should_receive(:variance).and_return(1)
        subject.sigma
      end

      it 'calculates the square root of the variance' do
        subject.stub!(:variance).and_return(42)
        Math.should_receive(:sqrt).once.with(42)
        subject.sigma
      end
    end

    describe '#s on a sample' do
      it 'calculates variance with a custom denominator' do
        subject.should_receive(:variance).with(subject.length - 1).and_return(1)
        subject.s
      end

      it 'calculates the square root of the variance' do
        subject.stub!(:variance).and_return(42)
        Math.should_receive(:sqrt).once.with(42)
        subject.s
      end
    end

    describe '#variance' do
      it 'calls method #mean' do
        subject.should_receive(:mean).once.and_return(1)
        subject.variance
      end

      it 'divides the mean by the length of all items by default' do
        spy = mock
        subject.stub!(:stddev_summation).and_return(spy)
        spy.should_receive(:/).once.with(subject.length)
        subject.variance
      end

      it 'divides the mean by an arbitrary number if provided' do
        spy = mock
        subject.stub!(:stddev_summation).and_return(spy)
        spy.should_receive(:/).once.with(4)
        subject.variance(4)
      end
    end
  end

  describe 'usage scenarios' do
    shared_examples_for 'an enumerable using Deviant' do
      it 'calculates the correct standard deviation for its data points' do
        subject.sigma.round.should == example_pop_stddev
      end
    end

    describe 'an array' do
      subject { example_data_set }
      it_should_behave_like 'an enumerable using Deviant'
    end

    describe 'any enumerable' do
      subject do
        # This is an anonymous class which implements #each.
        # Deviant works on *any* enumerable whose values respond to #to_f.
        Class.new do
          include Deviant
          
          def initialize(*args)
            @members = args
          end
          
          def each(&block)
            @members.each { |m| yield m } if block_given?
          end
        end.new(*example_data_set)
      end

      it_should_behave_like 'an enumerable using Deviant'
    end
  end
end
