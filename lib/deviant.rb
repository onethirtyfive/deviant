# encoding: utf-8

module Deviant
  def self.included(receiver)
    receiver.send :include, InstanceMethods
    receiver.send :include, ::Enumerable
  end

  module InstanceMethods
    def sigma
      Math.sqrt(variance)
    end
    alias_method :population_stddev, :sigma
    alias_method :𝜎, :sigma

    def s
      variance = self.variance(to_a.length - 1)
      Math.sqrt(variance)
    end
    alias_method :sample_stddev, :s
    alias_method :𝑠, :s

    def variance(denominator = nil)
      # calculate the mean just once
      mean = self.mean
      stddev_summation(mean) / (denominator || to_a.length)
    end

    def mean
      # gather numeric representations of array values just once
      sum = to_a.inject(0, :+)
      sum / to_a.length
    end

    private

    def stddev_summation(mean)
      to_a.inject(0) do |acc, numeric|
        acc += (numeric - mean)**2
        acc
      end
    end
  end
end

Dir[File.dirname(__FILE__) + '/deviant/*.rb'].each do |file|
  require file
end
