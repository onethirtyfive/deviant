# encoding: utf-8

module StandardDeviation
  module TableHeaderMappers
    def puppies_table_headers
      {
        /owner/i  => :owner,
        /name/i   => :name,
        /height/i => :height,
        /breed/i  => :breed
      }.freeze
    end
  end

  module Pets
    attr_accessor :pets

    def pet_height_data_points
      pets.values.flatten.collect { |pet| pet[:height].to_f }
    end

    def pet_height_data_points_for(owner)
      pets[owner].collect { |pet| pet[:height].to_f }
    end

    def method_for(mode)
      lookup = {
        'population' => :𝜎,
        'sample'     => :𝑠
      }
      lookup[mode]
    end
  end
end
