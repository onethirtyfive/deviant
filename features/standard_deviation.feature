# This feature is written to fulfill the examples given in
# http://www.mathsisfun.com/data/standard-deviation.html
#
# Normally, I'm dead serious with features, but I'm going
# to have a bit of fun with this one, since this should be
# a) unique and b) creative.

Feature: Standard deviation
  As a dog enthusiast, stats geek, and Rubyist
  I want to know the standard deviation of height amongst some puppies
  In order to scratch a peculiar intellectual itch

Background:
  Given two owners "Joshua" and "Sarah"
  And the following pet puppies:
    | Owner   | Name       | Height | Breed             |
    |  Joshua |  Newbery   |  170mm |  Dachshund        |
    |  Joshua |  Hurricane |  300mm |  French Bulldog   |
    |  Joshua |  Sparky    |  430mm |  Scottish Terrier |
    |  Sarah  |  Radish    |  470mm |  Mutt             |
    |  Sarah  |  Pheobe    |  600mm |  Rottweiler       |

Scenario: Standard deviation on all puppy heights as population
  Given an array containing the heights of all puppies
  When I ask it for its standard deviation as a population
  Then it should return an approximate value of 147

Scenario: Standard deviation on all puppy heights as sample
  Given an array containing the heights of all puppies
  When I ask it for its standard deviation as a sample
  Then it should return an approximate value of 164

Scenario: Calculating standard deviation of height on Joshua's puppies
  Given an array containing the heights of Joshua's puppies only
  When I ask it for its standard deviation as a sample
  Then it should return an approximate value of 130

