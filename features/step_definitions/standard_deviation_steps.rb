World(StandardDeviation::TableHeaderMappers)
World(StandardDeviation::Pets)

Given /^two owners "([^"]*)" and "([^"]*)"$/ do |owner1, owner2|
  # no-op. we could use it to check values in the next step,
  # but that would be overkill. we're not writing 'owner' features.
end

Given /^the following pet puppies:$/ do |puppies_table|
  puppies_table.map_headers!(puppies_table_headers)
  self.pets = puppies_table.hashes.each_with_object({}) do |puppy, pets|
    owner = puppy.delete(:owner)
    pets[owner] ||= []
    pets[owner] << puppy
  end
end

Given /^an array containing the heights of all puppies$/ do
  @array = self.pet_height_data_points
end

Given /^an array containing the heights of ([^']+)'s puppies only$/ do |owner|
  @array = self.pet_height_data_points_for(owner)
end

When /^I ask it for its standard deviation as a (population|sample)$/ do |mode|
  @stddev = @array.send(self.method_for(mode))
end

Then /^it should return an approximate value of (\d+)$/ do |value|
  @stddev.floor.should == value.to_i
end

